import "./App.css";

function App() {
  return (
    <div className="App">
      <section className="hero-section">
        <div className="hero-copy">
          <h1>Hey there!</h1>
          <p>Lets do some image optimisation!</p>
        </div>

        <div className="hero-image">
          <picture>
            <source
              srcSet="/avif/andrew-1600w.avif 1600w,
                      /avif/andrew-800w.avif 800w,
                      /avif/andrew-400w.avif 400w,
                      /avif/andrew-200w.avif 200w"
              sizes="(min-width: 768px) 50vw,
                    100vw"
              type="image/avif"
            />
            <source
              srcSet="/webp/andrew-1600w.webp 1600w,
                      /webp/andrew-800w.webp 800w,
                      /webp/andrew-400w.webp 400w,
                      /webp/andrew-200w.webp 200w"
              sizes="(min-width: 768px) 50vw,
                    100vw"
              type="image/webp"
            />
            <img
              src="/png/andrew-1600w.png"
              srcSet="/png/andrew-1600w.png 1600w,
                      /png/andrew-800w.png 800w,
                      /png/andrew-400w.png 400w,
                      /png/andrew-200w.png 200w"
              sizes="(min-width: 768px) 50vw,
                    100vw"
              alt=""
              className="responsive-image"
            />
          </picture>
        </div>
      </section>
    </div>
  );
}

export default App;
